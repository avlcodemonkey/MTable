# MTable
Table component using MithrilJS. 

- Offers searching, (multi)sorting, paging, resizable columns. 
- Loads JSON data from URL.
- Remembers all settings using localStorage.
- Demo at http://avlcodemonkey.gitlab.io/MTable/
