/*!
 * Mithril based table component. Supports ajax data, searching, sorting, paging, & resizing columns.
 */
(function (factory) {
    // Assume a traditional browser.
    window.MTable = factory($);
})(function ($) {
    'use strict';

    /**
     * Declare MTable class.
     * @param {Object} opts - Table settings
     */
    function MTable(opts) {
        opts = opts || {};
        this.opts = $.extend({
            container: null,
            id: null,
            columns: null,
            url: '',
            requestMethod: 'GET',
            searchable: true,
            loadAllData: true,
            inputTimeout: 200,
            columnMinWidth: 50,
            width: 100,
            editable: true,
            pageDropdown: true,
            headerButtons: null,
            storageFunction: null,
            itemsPerPage: null,
            searchQuery: null,
            currentStartItem: null,
            sorting: null,
            dataCallback: null,
            errorCallback: null,
            dataDateFormat: 'YYYY-MM-DD HH:mm:ss',
            displayDateFormat: 'YYYY-MM-DD HH:mm',
            displayCurrencyFormat: '{s:$} {[t:,][d:.][p:2]}',
            resources: {
                firstPage: 'First Page',
                previousPage: 'Previous Page',
                nextPage: 'Next Page',
                lastPage: 'Last Page',
                noData: 'There are no results.',
                showing: 'Showing {0} - {1} of {2}',
                page: 'Page',
                perPage: 'Per Page',
                loadingError: 'There was a problem loading this table.',
                tryAgain: 'Try Again'
            }
        }, opts);

        this.container = $.get(this.opts.container);
        this.layoutSet = false;
        this.data = null;
        this.loading = true;
        this.loadingError = false;
        this.filteredTotal = 0;
        this.results = [];
        this.pageTotal = 0;
        this.totalDistance = 0;
        this.lastSeenAt = { x: null, y: null };
        this.columnRenderer = {};
        this.colGroups = [];
        this.events = {};
        this.intColumns = [];
        this.dateColumns = [];
        this.currencyColumns = [];

        var self = this;
        for (var i = 0; i < this.opts.columns.length; i++) {
            var column = this.opts.columns[i];
            column.width = $.hasPositiveValue(column.width) ? column.width : this.store(column.field + ".width");
            column.dataType = $.coalesce(column.dataType, 'string');

            var columnFunction = $.isNull(column.links) || column.links.length === 0 ?
                function (obj, column, index) { return self.getDisplayValue(obj[column.field], column.dataType.toLowerCase()); } :
                function (obj, column, index) {
                    return column.links.map(function (link, j) {
                        var label = $.coalesce(link.label, self.getDisplayValue(obj[column.field], column.dataType.toLowerCase()));
                        var attr = $.clone(link.attributes) || {};
                        var isBtn = attr.classList && attr.classList.indexOf('btn') > -1;

                        var href = link.href || null;
                        if (href) {
                            for (var prop in obj) {
                                if (href.indexOf('{' + prop + '}') > -1 && obj.hasOwnProperty(prop)) {
                                    href = href.replace(new RegExp('{' + prop + '}', 'g'), obj[prop]);
                                }
                            }
                        }
                        attr[isBtn ? 'data-href' : 'href'] = href;
                        return m(isBtn ? 'button' : 'a', attr, $.isNull(link.icon) ? label : m('i', { class: 'fa fa-' + link.icon.toLowerCase(), title: label }));
                    });
                };

            this.columnRenderer[column.field] = columnFunction;
            this.colGroups.push(m('col'));

            var type = (column.dataType || 'string').toLowerCase();
            if (type === 'int') {
                this.intColumns.push(column.field);
            } else if (type === 'date') {
                this.dateColumns.push(column.field);
            } else if (type === 'currency') {
                this.currencyColumns.push(column.field);
            }
        }

        this.itemsPerPage = this.store('itemsPerPage') * 1 || 10;
        this.currentStartItem = this.store('currentStartItem') * 1 || 0;
        this.searchQuery = this.store('searchQuery') || '';
        this.width = this.store('width') * 1 || 100;
        var sorting = this.store('sorting');
        this.sorting = (typeof sorting === 'string' ? JSON.parse(sorting) : sorting) || [];

        this.run();
    }

    MTable.prototype = {
        pageOptions: [m('option', { value: '10' }, '10'), m('option', { value: '20' }, '20'), m('option', { value: '50' }, '50'), m('option', { value: '100' }, '100')],

        /**
         * Get/set persistent values.
         * @param {string} key - Key name of the value to get/set.
         * @param {*} value - Value to set.
         * @returns {string|undefined} Value if getting, else undefined.
         */
        store: function (key, value) {
            var myKey = this.opts.id + '.' + key;
            // getter
            if (typeof value === 'undefined') {
                return $.isNull(this.opts.storageFunction) ? localStorage[myKey] : $.coalesce(this.opts[key], null);
            }

            // setter
            if ($.isNull(this.opts.storageFunction)) {
                localStorage[myKey] = value;
            } else {
                // ignore the param actually passed and use the values from the object
                // passing the whole object lets the storage function decide when to actually save using a debounce
                if ($.isFunction(this.opts.storageFunction)) {
                    this.opts.storageFunction({
                        itemsPerPage: this.itemsPerPage,
                        currentStartItem: this.currentStartItem,
                        searchQuery: this.searchQuery,
                        width: this.width,
                        sorting: this.sorting,
                        columnWidths: this.opts.columns.map(function (c) { return { field: c.field, width: c.width * 1.0 }; })
                    });
                }
            }
        },

        /**
         * Process the data array result from the ajax request.
         * @param {Object[]} data - Array of records to display.
         */
        processData: function (data) {
            if (this.opts.dataCallback) {
                this.opts.dataCallback(data);
            }

            var i = 0, len = data.rows.length, j = 0;
            for (; i < len; i++) {
                // add an index to the data so we can reset to the default sort order later if the user wants
                data.rows[i]._index = i;

                var x;
                // convert input to appropriate types
                for (j = 0; j < this.intColumns.length; j++) {
                    x = this.intColumns[j];
                    data.rows[i][x] = $.isNull(data.rows[i][x]) ? null : data.rows[i][x] * 1;
                }
                for (j = 0; j < this.dateColumns.length; j++) {
                    x = this.dateColumns[j];
                    data.rows[i][x] = $.isNull(data.rows[i][x]) ? null : fecha.parse(data.rows[i][x], this.opts.dataDateFormat);
                }
                for (j = 0; j < this.currencyColumns.length; j++) {
                    x = this.currencyColumns[j];
                    data.rows[i][x] = $.isNull(data.rows[i][x]) ? null : accounting.unformat(data.rows[i][x]);
                }
            }
            this.data = data.rows;
            this.filteredTotal = data.filteredTotal;

            this.loading = false;
            this.sort(false);
            this.filterResults();
        },

        /**
         * Load the data to populate the table.
         */
        loadData: function () {
            this.loading = true;
            this.loadingError = false;

            var self = this;
            $.ajax({
                method: this.opts.requestMethod,
                url: this.opts.url,
                data: this.buildParams(),
                block: false
            }, this.processData.bind(this), function (data) {
                self.loading = false;
                self.loadingError = true;
                if (self.opts.errorCallback) {
                    self.opts.errorCallback(data);
                }
            });
        },

        /**
         * Force the table to refresh its data.
         */
        refresh: function () {
            this.loading = true;
            this.loadingError = false;
            this.loadData();
        },

        /**
         * Build querystring params to fetch data from the server.
         * @returns {Object} Request parameters.
         */
        buildParams: function () {
            return {
                startItem: this.currentStartItem,
                items: this.itemsPerPage,
                query: this.searchQuery,
                sort: this.sorting.length > 0 ? this.sorting.map(function (obj, i) { return { field: obj.field, dir: obj.dir, index: i }; }) : null,
                t: Math.random()
            };
        },

        /**
         * Set the first item index to display.
         * @param {type} index - Record index to start on.
         */
        setCurrentStartItem: function (index) {
            this.currentStartItem = index;
            this.store('currentStartItem', index);
            this.filterResults(true);
        },

        /**
         * Set the number of items to display per page.
         * @param {number|Event} e - Number or items per page, or an event that triggered the change.
         */
        setItemsPerPage: function (e) {
            if (this.loading) {
                return;
            }

            var items = (isNaN(e) ? e.target.value : e) * 1;
            if (this.itemsPerPage !== items) {
                this.itemsPerPage = items;
                this.store('itemsPerPage', items);
                this.setCurrentStartItem(0);
            }
        },

        /**
         * Set the search query for filtering table data.
         * @param {string} val - New search text.
         */
        setSearchQuery: function (val) {
            if (this.loading) {
                return;
            }

            var query = val.target ? val.target.value : val;
            if (this.searchQuery !== query) {
                this.searchQuery = query;
                clearTimeout(this.requestTimer);
                this.requestTimer = setTimeout(this.runSearch.bind(this, query), this.opts.inputTimeout);
            }
        },

        /**
         * Change search query and filter results.
         * @param {string} query - New search text.
         */
        runSearch: function (query) {
            this.store('searchQuery', query);
            this.requestTimer = null;
            this.currentStartItem = 0;
            this.filterResults(true);
            m.redraw();
        },

        /**
         * Filter the data based on the search query, current page, and items per page.
         * @param {bool} refresh - Force it to refresh its data.
         */
        filterResults: function (refresh) {
            if (this.loading) {
                return;
            }

            if (refresh && !this.opts.loadAllData) {
                // force the data to reload. filterResults will get called again after the data loads
                this.loadData();
            } else if (!this.opts.loadAllData) {
                // we're not loading all the data to begin with. so whatever data we have should be displayed.
                this.results = this.data;
                this.pageTotal = Math.ceil(this.filteredTotal / this.itemsPerPage);
            } else {
                // we're loading all the data to begin with. so figure out what data to display.
                var filteredTotal = 0;
                if (this.data.constructor !== Array) {
                    this.loading = true;
                    this.results = [];
                } else {
                    var startItem = this.currentStartItem;
                    var res = this.searchQuery ? this.data.filter(this.filterArray.bind(this.searchQuery.toLowerCase())) : this.data;
                    filteredTotal = res.length;
                    this.results = res.slice(startItem, startItem + this.itemsPerPage);
                }

                this.filteredTotal = filteredTotal;
                this.pageTotal = Math.ceil(filteredTotal / this.itemsPerPage);
            }
        },

        /**
         * Page forward or backward. 
         * @param {number} d - Direction to move, 1 is forward, -1 is backward.
         * @param {number} m - Move to end (first or last page) if true.
         */
        moveToPage: function (d, m) {
            this.changePage(d === 1 ? m ? this.pageTotal : this.currentStartItem / this.itemsPerPage + 2 : m ? 1 : this.currentStartItem / this.itemsPerPage);
        },

        /**
         * Move to the specified page number.
         * @param {number|Event} e - New page number, or an event that triggered the change.
         */
        changePage: function (e) {
            if (this.loading) {
                return;
            }

            var page = (isNaN(e) ? e.target.value : e) * 1;
            if (page <= this.pageTotal && page > 0) {
                this.setCurrentStartItem((page - 1) * this.itemsPerPage);
            }
        },

        /**
         * Change the sorting order.
         * @param {string} fieldName - Name of the field to sort on.
         * @param {string} dataType - Data type of the field.
         * @param {Event} e - Event that triggered the change.
         */
        changeSort: function (fieldName, dataType, e) {
            if (this.loading) {
                return;
            }

            var val = $.getKVP(this.sorting, 'field', fieldName);
            if (e.shiftKey) {
                document.getSelection().removeAllRanges();
            } else {
                this.sorting = [];
            }

            if (val === null) {
                this.sorting.push({ field: fieldName, dir: 'ASC', dataType: dataType || 'string' });
            } else if (e.shiftKey) {
                if (val.dir === 'DESC') {
                    this.sorting.splice(val._i, 1);
                } else {
                    val.dir = 'DESC';
                    this.sorting[val._i] = val;
                }
            } else {
                val.dir = val.dir === 'ASC' ? 'DESC' : 'ASC';
                this.sorting.push(val);
            }

            this.sort();
            this.setCurrentStartItem(0);
        },

        /**
         * Sort the underlying data.
         * @param {bool} refresh - Refresh the data from the server.
         */
        sort: function (refresh) {
            refresh = $.coalesce(refresh, true);
            this.data.sort(this.sorting.length > 0 ? this.compare.bind(this) : this.defaultCompare);
            this.filterResults(refresh);
            this.store('sorting', JSON.stringify(this.sorting));
        },

        /**
         * Set up the table and column styles and events.
         */
        setLayout: function () {
            if (this.layoutSet) {
                return;
            }

            this.layoutSet = true;
            this.container = $.get(this.opts.container);
            this.table = $.get('#' + this.opts.id, this.container);
            this.table.style.tableLayout = 'fixed';
            this.tableHeaderRow = this.table.tHead.rows[0];
            this.top = $.get('.table-scrollable', this.container);
            this.columnGroup = $.get('.table-column-group', this.container);

            if (this.table !== null) {
                this.clientWidth = this.container.clientWidth;
                this.table.tHead.style.width = this.table.style.width = (this.width / 100 * this.table.offsetWidth) + "px";

                var hWidth = this.table.tHead.offsetWidth;
                var tWidth = this.table.offsetWidth;
                for (var i = 0; i < this.opts.columns.length; i++) {
                    if (!this.opts.columns[i].width) {
                        this.opts.columns[i].width = this.tableHeaderRow.cells[i].offsetWidth / hWidth * 100;
                    }
                    this.tableHeaderRow.cells[i].style.width = this.opts.columns[i].width / 100 * tWidth + "px";
                }

                if (this.opts.editable) {
                    this.events = { resize: this.onResize.bind(this), move: this.onMouseMove.bind(this), up: this.onMouseUp.bind(this) };
                    $.on(window, 'resize', $.debounce(this.events.resize, 100));
                    var topNode = $.closest('.ajs-content', this.table) || window;
                    $.on(topNode, 'mousemove', this.events.move);
                    $.on(topNode, 'mouseup', this.events.up);

                    var header = $.get('thead', this.table);
                    if (header) {
                        $.on(header, 'touchstart', this.touchHandler.bind(this));
                        $.on(header, 'touchmove', this.touchHandler.bind(this));
                        $.on(header, 'touchend', this.touchHandler.bind(this));
                        $.on(header, 'touchcancel', this.touchHandler.bind(this));
                    }
                }
            }
        },

        /**
         * Clean up our mess.
         */
        destroy: function () {
            if (this.opts.editable) {
                var topNode = $.closest('.ajs-content', this.table) || window;
                $.off(topNode, 'resize', this.events.resize);
                $.off(topNode, 'mousemove', this.events.move);
                $.off(topNode, 'mouseup', this.events.up);
            }
            m.mount(this.container, null);
        },

        /**
         * Update the table and column widths based on a window resize.
         */
        onResize: function () {
            var scale = this.container.clientWidth / this.clientWidth;
            this.clientWidth = this.container.clientWidth;
            this.table.tHead.style.width = this.table.style.width = (this.pixelToFloat(this.table.style.width) * scale) + 'px';
            for (var i = 0; i < this.opts.columns.length; i++) {
                this.tableHeaderRow.cells[i].style.width = (this.pixelToFloat(this.tableHeaderRow.cells[i].style.width) * scale) + 'px';
            }
            this.updateLayout();
        },

        /**
         * Update the table header style.
         */
        updateLayout: function () {
            if (this.table.offsetParent === null) {
                return;
            }
            this.top.style.paddingTop = this.table.tHead.offsetHeight + 'px';
            for (var i = 0; i < this.opts.columns.length; i++) {
                this.columnGroup.children[i].style.width = this.tableHeaderRow.cells[i].style.width;
            }
            if (this.clientWidth > 0 && this.container.clientWidth / this.clientWidth !== 1) {
                this.onResize();
            }
        },

        /**
         * Make the table header scroll horizontally with the table
         * @param {Event} e - Event that triggered the scroll.
         */
        onScroll: function (e) {
            var head = this.table.tHead;
            var scroll = e.target;
            if (-head.offsetLeft !== scroll.scrollLeft) {
                head.style.left = "-" + scroll.scrollLeft + "px";
            }
        },

        /**
         * Handle dragging to change column widths.
         * @param {type} e - Event that triggered the change.
         */
        onHeaderMouseDown: function (e) {
            if (e.button !== 0) {
                return;
            }

            var self = this;
            var callbackFunc = function (cellEl) {
                e.stopImmediatePropagation();
                e.preventDefault();

                self.resizeContext = {
                    colIndex: cellEl.cellIndex,
                    initX: e.clientX,
                    scrWidth: self.top.offsetWidth,
                    initTblWidth: self.table.offsetWidth,
                    initColWidth: self.pixelToFloat(self.columnGroup.children[cellEl.cellIndex].style.width),
                    layoutTimer: null
                };
            };
            self.inResizeArea(e, callbackFunc);
        },

        /**
         * Handle resizing columns.
         * @param {Event} e - Event that triggered the change.
         */
        onMouseMove: function (e) {
            var newStyle = '';
            var cursorFunc = function () {
                newStyle = 'col-resize';
            };
            this.inResizeArea(e, cursorFunc);
            if (this.table.tHead.style.cursor !== newStyle) {
                this.table.tHead.style.cursor = newStyle;
            }

            var ctx = this.resizeContext;
            if (typeof ctx === 'undefined' || ctx === null) {
                return;
            }

            e.stopImmediatePropagation();
            e.preventDefault();

            var newColWidth = Math.max(ctx.initColWidth + e.clientX - ctx.initX, this.opts.columnMinWidth);
            this.table.tHead.style.width = this.table.style.width = (ctx.initTblWidth + (newColWidth - ctx.initColWidth)) + "px";
            this.columnGroup.children[ctx.colIndex].style.width = this.tableHeaderRow.cells[ctx.colIndex].style.width = newColWidth + "px";

            if (ctx.layoutTimer === null) {
                var self = this;
                var timerFunc = function () {
                    self.resizeContext.layoutTimer = null;
                    self.updateLayout();
                };
                ctx.layoutTimer = setTimeout(timerFunc, 25);
            }
        },

        /**
         * Update column widths and save them.
         * @param {Event} e - Event that triggered.
         */
        onMouseUp: function (e) {
            var ctx = this.resizeContext;
            if (typeof ctx === 'undefined' || ctx === null) {
                return;
            }

            if (ctx.layoutTimer !== null) {
                clearTimeout(ctx.layoutTimer);
            }
            this.resizeContext = null;

            var newTblWidth = this.table.offsetWidth;
            this.width = (newTblWidth / ctx.scrWidth * 100).toFixed(2);
            this.store('width', this.width);
            for (var i = 0; i < this.opts.columns.length; i++) {
                this.opts.columns[i].width = (this.pixelToFloat(this.tableHeaderRow.cells[i].style.width) / newTblWidth * 100).toFixed(2);
                this.store(this.opts.columns[i].field + ".width", this.opts.columns[i].width);
            }

            this.updateLayout();
        },

        /**
         * Check if the cursor is in the area where the user can click to drag a column.
         * @param {Event} e - Event that triggered the check.
         * @param {Function} callback - Function to run if in the resize area.
         */
        inResizeArea: function (e, callback) {
            var tblX = e.clientX;
            var el;
            for (el = this.table.tHead; el !== null; el = el.offsetParent) {
                tblX -= el.offsetLeft + el.clientLeft - el.scrollLeft;
            }

            var cellEl = e.target;
            while (cellEl !== this.table.tHead && cellEl !== null) {
                if (cellEl.nodeName === "TH") {
                    break;
                }
                cellEl = cellEl.parentNode;
            }

            if (cellEl === this.table.tHead) {
                for (var i = this.tableHeaderRow.cells.length - 1; i >= 0; i--) {
                    cellEl = this.tableHeaderRow.cells[i];
                    if (cellEl.offsetLeft <= tblX) {
                        break;
                    }
                }
            }

            if (cellEl !== null) {
                var x = tblX;
                for (el = cellEl; el !== this.table.tHead; el = el.offsetParent) {
                    if (el === null) {
                        break;
                    }
                    x -= el.offsetLeft - el.scrollLeft + el.clientLeft;
                }
                if (x < 5 && cellEl.cellIndex !== 0) {
                    callback.call(this, cellEl.previousElementSibling);
                } else if (x > cellEl.clientWidth - 5) {
                    callback.call(this, cellEl);
                }
            }
        },

        /**
         * Make column resizing play nice with touch. http://stackoverflow.com/questions/28218888/touch-event-handler-overrides-click-handlers
         * @param {Event} e Event that triggered the handler.
         */
        touchHandler: function (e) {
            var mouseEvent = null;
            var simulatedEvent = document.createEvent('MouseEvent');
            var touch = e.changedTouches[0];

            switch (e.type) {
                case 'touchstart':
                    mouseEvent = 'mousedown';
                    this.totalDistance = 0;
                    this.lastSeenAt.x = touch.clientX;
                    this.lastSeenAt.y = touch.clientY;
                    break;
                case 'touchmove':
                    mouseEvent = 'mousemove';
                    break;
                case 'touchend':
                    if (this.lastSeenAt.x) {
                        this.totalDistance += Math.sqrt(Math.pow(this.lastSeenAt.y - touch.clientY, 2) + Math.pow(this.lastSeenAt.x - touch.clientX, 2));
                    }
                    mouseEvent = this.totalDistance > 5 ? 'mouseup' : 'click';
                    this.lastSeenAt = { x: null, y: null };
                    break;
            }

            simulatedEvent.initMouseEvent(mouseEvent, true, true, window, 1, touch.screenX, touch.screenY, touch.clientX, touch.clientY, false, false, false, false, 0, null);
            touch.target.dispatchEvent(simulatedEvent);
            e.preventDefault();
        },

        /**
         * Get the value for the field coverted to the correct data type.
         * @param {string} value - Value to convert.
         * @returns {*} Converted value.
         */
        getFieldValue: function (value) {
            return value.getMonth ? value : value.toLowerCase ? value.toLowerCase() : value;
        },

        /**
         * Get the formatted value to display for the field.
         * @param {string} value - Value to format.
         * @param {string} dataType - Data type to format to.
         * @returns {*} Converted value.
         */
        getDisplayValue: function (value, dataType) {
            if (!dataType || $.isNull(value)) {
                return value;
            }

            var val = value;
            if (dataType === 'currency') {
                val = accounting.formatMoney(val, this.opts.displayCurrencyFormat);
            } else if (dataType === 'date') {
                val = fecha.format(val, this.opts.displayDateFormat);
            }
            return val;
        },

        /**
         * Default sorting function for the data - resets to order when data was loaded.
         * @param {Object} a - First object to compare.
         * @param {Object} b - Object to compare to.
         * @returns {number} 1 if a comes first, -1 if b comes first, else 0.
         */
        defaultCompare: function (a, b) {
            return a._index > b._index ? 1 : a._index < b._index ? -1 : 0;
        },

        /**
         * Multi-sorting function for the data.
         * @param {Object} a - First object to compare.
         * @param {Object} b - Object to compare to.
         * @returns {number} 1 if a comes first, -1 if b comes first, else 0.
         */
        compare: function (a, b) {
            var sorting = this.sorting;
            var i = 0, len = sorting.length;
            for (; i < len; i++) {
                var sort = sorting[i];
                var aa = this.getFieldValue(a[sort.field]);
                var bb = this.getFieldValue(b[sort.field]);

                if (aa < bb) {
                    return sort.dir === 'ASC' ? -1 : 1;
                }
                if (aa > bb) {
                    return sort.dir === 'ASC' ? 1 : -1;
                }
            }
            return 0;
        },

        /**
         * Filter an array of objects to find objects where value contains the value of `this`
         * @param {Object} obj - Object to search in.
         * @returns {bool} True if object contains `this`.
         */
        filterArray: function (obj) {
            for (var key in obj) {
                if (key.indexOf('_') < 0 && obj.hasOwnProperty(key) && (obj[key] + '').toLowerCase().indexOf(this) > -1) {
                    return true;
                }
            }
            return false;
        },

        /**
         * Convert a style with 'px' to a float.
         * @param {string} val - CSS style to convert.
         * @returns {number} Numeric value.
         */
        pixelToFloat: function (val) {
            return val.replace('px', '').replace('%', '') * 1.0;
        },

        /**
         * Build the table header tags.
         * @param {Object} obj - Column to build the tag for.
         * @returns {Object} Mithril TH node.
         */
        tableHeaders: function (obj) {
            var field = obj.field;
            var thAttrs = { className: obj.classes || '' }, divAttrs = {};

            var divContent = [obj.label || field];
            if (typeof obj.sortable === 'undefined' || obj.sortable === true) {
                var val = $.getKVP(this.sorting, 'field', field);
                divContent.push(m('i', { className: 'float-xs-right fa ' + (val ? (val.dir === 'ASC' ? 'fa-sort-up' : 'fa-sort-down') : this.opts.editable ? 'fa-sort' : '') }));
                if (this.opts.editable) {
                    thAttrs.onmousedown = this.onHeaderMouseDown.bind(this);
                    divAttrs = { onclick: this.changeSort.bind(this, field, obj.dataType.toLowerCase()) };
                }
            } else {
                thAttrs.class += ' disabled';
            }

            return m('th', thAttrs, [m('div', divAttrs, divContent)]);
        },

        /**
         * Build the view that actually shows the table.
         * @returns {Object}  Mithril DIV node.
         */
        view: function () {
            return m('div', { className: 'container-fluid mtable-container', id: this.opts.id + '-container' }, [
                !this.opts.editable ? m('span', { id: 'table-items-per-page' }) :
                    m('div', { className: 'row form-inline' }, [
                        m('div', { className: 'col-sm-6 col-xs-12 pb-1' }, [
                            m('div', { className: 'float-xs-left btn-toolbar' }, [
                                this.opts.searchable ? m('div', { className: 'input-group' }, [
                                    m('span', { className: 'input-group-addon' }, m('i', { className: 'fa fa-search' })),
                                    m('input', { type: 'text', className: 'form-control', oninput: this.setSearchQuery.bind(this), value: this.searchQuery, disabled: this.loading })
                                ]) : ''
                            ])
                        ]),
                        m('div', { className: 'col-sm-6 col-xs-12 float-xs-right pb-1' }, [
                            m('div', { className: 'float-xs-right btn-toolbar' }, [
                                m('div', { className: 'input-group' }, [
                                    m('span', { className: 'input-group-addon' }, this.opts.resources.perPage),
                                    m('select', {
                                        className: 'form-control custom-select', id: 'table-items-per-page', onchange: this.setItemsPerPage.bind(this),
                                        value: this.itemsPerPage, disabled: this.loading
                                    }, this.pageOptions)
                                ])
                            ]),
                            m('div', { className: 'float-xs-right pr-1 btn-toolbar' }, [
                                this.opts.headerButtons
                            ])
                        ])
                    ]),
                m('.row', [
                    m('div', { className: 'table-scrollable' + (this.opts.editable ? '' : ' table-no-edit') }, [
                        m('div', { className: 'table-area', onscroll: this.onScroll.bind(this) }, [
                            m('table', { className: 'table table-hover table-sm table-striped mtable-data-table', id: this.opts.id }, [
                                m('colgroup', { className: 'table-column-group' }, this.colGroups),
                                m('thead', [m('tr', this.opts.columns.map(this.tableHeaders.bind(this)))]),
                                m('tbody', this.tableBodyView())
                            ])
                        ])
                    ])
                ]),
                this.tableFooterView()
            ]);
        },

        /**
         * Build a single table cell.
         * @param {Object} obj - Table record to build cell for.
         * @param {number} index - Row index of this row.
         * @param {Object} column - Column to build cell for.
         * @returns {Object} Mithril TD node.
         */
        tableCellView: function (obj, index, column) {
            return m('td', this.columnRenderer[column.field](obj, column, index));
        },

        /**
         * Build a table row.
         * @param {Object} obj - Table record to build row for.
         * @param {number} index - Row index of this row.
         * @returns {Object} Mithril TR node.
         */
        tableRowView: function (obj, index) {
            return m('tr', { key: obj._index }, this.opts.columns.map(this.tableCellView.bind(this, obj, index)));
        },

        /**
         * Build the table footer nodes
         * @returns {Object} Mithril TR node(s).
         */
        tableBodyView: function () {
            if (this.loading) {
                return m('tr', m('td', { colspan: this.opts.columns.length, className: null }, m('div', { className: 'table-spinner' }, [
                    m('div', { className: 'rect1' }, ''), m('div', { className: 'rect2' }, ''), m('div', { className: 'rect3' }, ''),
                    m('div', { className: 'rect4' }, ''), m('div', { className: 'rect5' }, '')
                ])));
            }
            if (this.loadingError) {
                return m('tr', m('td', { colspan: this.opts.columns.length, className: 'table-loading-error' }, [
                    m('div', { className: 'table-loading-error-message' }, this.opts.resources.loadingError),
                    m('div', { className: 'btn btn-info btn-sm', onclick: this.refresh.bind(this) }, this.opts.resources.tryAgain)
                ]));
            }
            if (this.filteredTotal === 0) {
                return m('tr', [m('td', { colspan: this.opts.columns.length }, this.opts.resources.noData)]);
            }
            return this.results.map(this.tableRowView.bind(this));
        },

        /**
         * Build the table footer nodes.
         * @returns {Object} Mithril DIV node.
         */
        tableFooterView: function () {
            if (this.loading || this.loadingError) {
                return null;
            }

            var currentPage = (this.currentStartItem + this.itemsPerPage) / this.itemsPerPage;
            if (this.opts.pageDropdown) {
                // limit page dropdown to 10000 options
                var max = Math.min(this.pageTotal, 10000);
                var optionList = [max], i = max;
                while (i > 0) {
                    optionList[i] = m('option', { value: i }, i);
                    --i;
                }
            }

            var res = this.opts.resources;
            return m('div', { className: 'row text-center text-xs-center pt-1' }, [
                m('div', { className: 'col-sm-4 col-xs-12 ' + (this.filteredTotal > this.itemsPerPage ? '' : 'hide invisible') }, [
                    m('i', { className: 'fa fa-lg fa-pad fa-fast-backward btn', alt: res.firstPage, title: res.firstPage, onclick: this.moveToPage.bind(this, -1, true) }),
                    m('i', { className: 'fa fa-lg fa-pad fa-step-backward btn', alt: res.previousPage, title: res.previousPage, onclick: this.moveToPage.bind(this, -1, false) }),
                    m('i', { className: 'fa fa-lg fa-pad fa-step-forward btn', alt: res.nextPage, title: res.nextPage, onclick: this.moveToPage.bind(this, 1, false) }),
                    m('i', { className: 'fa fa-lg fa-pad fa-fast-forward btn', alt: res.lastPage, title: res.lastPage, onclick: this.moveToPage.bind(this, 1, true) })
                ]),
                m('div', { className: 'col-sm-4 form-inline hidden-xs ' + (this.filteredTotal > this.itemsPerPage ? '' : 'hide invisible') },
                    !this.opts.pageDropdown ? null : [
                        m('div', { className: 'mx-auto' }, [
                            m('div', { className: 'input-group' }, [
                                m('span', { className: 'input-group-addon' }, res.page),
                                m('select', { className: 'form-control custom-select', onchange: this.changePage.bind(this), value: currentPage }, optionList)
                            ])
                        ])
                    ]
                ),
                m('div', { className: 'col-sm-4 col-xs-12 text-right float-xs-right' }, res.showing
                    .replace('{0}', Math.min(this.currentStartItem + 1, this.filteredTotal))
                    .replace('{1}', Math.min(this.currentStartItem + this.itemsPerPage, this.filteredTotal))
                    .replace('{2}', this.filteredTotal)
                )
            ]);
        },

        /**
         * Render the view.
         */
        run: function () {
            var self = this;
            self.loadData();
            m.mount(self.container, {
                view: self.view.bind(self),
                onupdate: function () {
                    self.setLayout();
                    self.updateLayout();
                }
            });
        }
    };

    return MTable;
});